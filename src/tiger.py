#!/usr/bin/env python
import subprocess
from time import sleep
import RPi.GPIO as GPIO
from sets import Set
import syslog
import os

#Definitions
yellowLedPin = 17
greenLedPin = 23
buttonPin = 25
waitingTime = 0.2
mp3File = '/home/pi/tiger/eye.mp3'

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
#yellow led
GPIO.setup(yellowLedPin, GPIO.OUT)
#green led
GPIO.setup(greenLedPin, GPIO.OUT)
#button
GPIO.setup(buttonPin, GPIO.IN)

#yellow led on if started
GPIO.output(yellowLedPin, False)
#green led off
GPIO.output(greenLedPin, True)
#Log
syslog.syslog('Raspberry Tiger started')

#running process tally
processes = Set([])

last = GPIO.input(buttonPin)

while True:

	if last != GPIO.input(buttonPin):
		last = GPIO.input(buttonPin)

		#always stop all the music on key change. 
		os.system("killall mpg321")
		processes = Set([])
		GPIO.output(greenLedPin, True)

		if  GPIO.input(buttonPin) == 0:
			#Fresh press!
			sts = subprocess.Popen(['mpg321', mp3File])
			processes.add(sts.pid)
			GPIO.output(greenLedPin, False)
									
	#Finished playing
	if len(processes) > 0:
		try:
			if sts.poll() != None:
				processes.remove(sts.pid)
				GPIO.output(greenLedPin, True)
				syslog.syslog("Done playing song")
		except:
			pass
					
	#Wait 0.05 sec
	sleep(waitingTime);
	
#sjekk logg
#tail -f /var/log/syslog | grep tiger.py 
#edit startup
#sudo nano /etc/rc.local
#kill all
#sudo killall my_project.a
